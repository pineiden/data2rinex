#!/bin/bash

REPETIDOS=($(ls *.T0[0-2] | awk -F'.' '{print substr($1,1,length($1)-5)}'  | sort  | uniq -c | awk '{if($1>1) print $2}'))

for file in ${REPETIDOS[@]}; do
	this_lista=($(ls -la $file*.T0[0-2] | awk '{print $5"|"$9}'))
	#obtener valor inicial de comparacion
	value=$(echo ${this_lista[0]}| awk -F'|' '{print $1}')
	file=$(echo ${this_lista[0]}| awk -F'|' '{print $2}')	
	for this in ${this_lista[@]}; do 		
		new_value=$(echo $this| awk -F'|' '{print $1}')
		if [ $new_value > $value ]; 
		then
			mv $file repetidos/$file
			value=$new_value
			file=$(echo $this_lista| awk -F'|' '{print $2}')
		fi
	done
done
