
import sys, os, time
from libs.data2ascii import data2ascii
from libs.asciirinex2compress import asciirinex2compress

# El script requiere como input un path y recorre el mismo, junto a todas sus sub carpetas, aplicando a todo archivo
# binario que encuentre T02, T01 T00 los programas para llevarlo a un CRNX comprimido en .Z.
# Los binarios se asumen en el formato  STATYYYYMMDDHHMMSS%.T0*, donde % es una letra y * un numero entre 0 y 2.
# El archivo de output tiene el formato  STATDOY0.YYo los archivos de configuracion deben estar en el
#  mismo directorio de los datos binarios


def fileExt(files): # Obtiene la extension de cada archivo (3 caracteres al final)
    return files[-3:].lower()

#Input PATH de archivos T02 con .conf
path = str(sys.argv[1])
control2rinex = str(sys.argv[2])
rinex2z = str(sys.argv[3])

print(path)
print(control2rinex)
print(rinex2z)
#Tiempo conteo operacion
start = time.time()

#En caso de existir path -> operar 
#F_OK verifica existencia
#https://docs.python.org/3/library/os.html
if control2rinex in ["-2rinex"]:
	data2ascii(path)

end1 = time.time()

#Nuevamente, si opera en directorio
if rinex2z in ["-compress"]:
	print(u"Compressing...")
	cont = asciirinex2compress(path)

end2 = time.time()

t_primera_op = end1 -start
t_segunda_op = end2 - end1
finaltime = end2 - start

print("The execution of "+str(cont)+" files took the script "+str(finaltime)+" seconds")
