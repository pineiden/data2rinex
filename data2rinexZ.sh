#!/bin/bash


unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    *)          machine="UNKNOWN:${unameOut}"
esac
echo ${machine}

echo $(pwd)
archivos_repetidos

procesando(){
    echo Procesando archivo $1
}
rinex2compress(){
    
    #parameter input
    echo "Generando compresión"
    configfile=$1
    dat_file=$2
    filename=$3
    rinexfilename=$4
    
    teqc -config $configfile -tr d $dat_file > $filename
    new_filename="1."$filename
    teqc -O.int 1 -O.dec 15 $filename > $new_filename
    mv -u $new_filename $filename
    RNX2CRX $filename
    compress $rinexfilename
}

#Starting value
ncpu=4

if [[ -n $1 ]]
then
    ncpu=$1
fi

Ncpu=$ncpu
#8
echo $Ncpu

echo "Filtrando archivos incorrectos"
#Find bad files and move
ls -la *.T0[0-2] | awk '{ if($5<5000) { print $9}}' | xargs -I input mv input fallados/*

echo "Convirtiendo a tgd o dat"
#converto to ascii .dat 
ls *.T0[0-2] | parallel runpkr00 -g -d
DAT_FILE=($(ls *.{tgd,dat}))

echo "Define listname"
#Define list names
FILENAME=($(ls *.{tgd,dat}|awk -v var="$machine" -F'.' '{
	estacion=tolower(substr($1,1,4)); 
	year=substr($1,5,4); 
	month=substr($1,9,2);
	day=substr($1,11,2);
	fecha=year"-"month"-"day;
  if (machine=="Mac"){
  command="gdate -u --date=\""fecha"\" +%j"}
  }
  else {
	command="date -u --date=\""fecha"\" +%j"}; 
	command | getline doy;
	close(command);
	print estacion doy"0"cola"."substr(year,3,2)"o"}'
))

RINEXFILENAME=($(ls *.{tgd,dat}|awk -v var="$machine"  -F'.' '{
	estacion=tolower(substr($1,1,4));
    year=substr($1,5,4);
    month=substr($1,9,2);
    day=substr($1,11,2);
    fecha=year"-"month"-"day;
    if (machine=="Mac"){
    command="gdate -u --date=\""fecha"\" +%j"}
    }
    else {
    command="date -u --date=\""fecha"\" +%j"}; 
    command | getline doy;
    close(command);
    print estacion doy"0"cola"."substr(year,3,2)"d"}'
))
CONFFILE=($(ls *.{tgd,dat}|awk -F'.' '{print substr($1,1,4)"_teqc.conf"}'))

export -f rinex2compress

for i in ${!DAT_FILE[*]}; do
   procesando $i
   echo ${CONFFILE[$i]} ${DAT_FILE[$i]} ${FILENAME[$i]} ${RINEXFILENAME[$i]}
   sem -j $Ncpu rinex2compress ${CONFFILE[$i]} ${DAT_FILE[$i]} ${FILENAME[$i]} ${RINEXFILENAME[$i]}
done
sem --wait
echo All done
