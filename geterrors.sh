
#!/bin/bash

RAW=($(ls *.T0[0-2] | awk  -F'.' '{print substr($1,0,length($1)-1)}'))
ZDATA=($(ls *.Z | awk  -F'.' '{print substr($1,0,length($1)-2)}'))


for z in ${ZDATA[@]}; do
    for r in ${RAW[@]}; do
        if [ "$z" == "$r" ];
            then
            NEW_RAW=( ${RAW[@]/$r} )    
        fi    
    done
    RAW=( ${NEW_RAW[@]} )
done

rm error.log

for each in "${NEW_RAW[@]}"
do
        echo  $each >> error.log
done


