import sys, os, time
from newformat import newformatname # Recibe STATYYYYMMDDHHMMX y entrega STATDOY0.YYo
from configfiles import chooseconfigfile # Determina que archivo de conf utilizar para cada estacion y fecha

def fileExt(files): # Obtiene la extension de cada archivo (3 caracteres al final)
    return files[-3:].lower()

def asciirinex2compress(path):
    cont = 0
    if os.access(path, os.F_OK):
        #Se itera en el directorio
        for root, dirs, files in os.walk(path):
            #os.system("cd " + root + ' && rm *.??d')

            #se itera en los archivos
            for eachfile in files:
                #Se verifica que exista .dat
                if fileExt(eachfile) in ["dat"]:
                    #conteo sube 1
                    cont = cont + 1
                    configfile = eachfile[0:4] + '_teqc.conf'
                    datname = eachfile[:-3]+"dat" #datname = eachfile  
                    print(datname)
                    newname = newformatname(eachfile).lower()
                    print(newname)
                    #operar el comando teqc
                    os.system('cd ' + root + ' && teqc -config ' + configfile  + " -tr d " + datname + " >" + newname )

                    os.system('cd ' + root + ' && teqc -O.int 1 -O.dec 15 ' + newname + ">" + "1." + newname )

                    os.system('cd ' + root + ' && cat 1.'+newname+">"+newname)

                    os.system('cd ' + root + ' && RNX2CRX ' + newname )

                    os.system("cd " + root + ' && compress ' + newname)
                    os.system("cd " + root + ' && rm ' + datname)
                    #os.system("cd " + root + ' && gzip -f -S .Z ' + newname[:-1]+"d")
                    # os.system("cd " + root + ' && rm ' + newname[:-1]+"d")
    return cont    
