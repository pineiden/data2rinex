from newformat import dayOfYear

def station(files):
    return files[:4].lower()

def past(year, month, day, year1, month1, day1):
    return int(year) > year1 or (int(year) == year1 and \
                                  dayOfYear(year, month, day) >= dayOfYear(year1, month1, day1))

def between(year, month, day, year1, month1, day1, year2, month2, day2):
    return past(year, month, day, year1, month1, day1) and not past(year, month, day, year2, month2, day2)

def chooseconfigfile(nameoffile):
    year = int(nameoffile[4:8])
    month = int(nameoffile[8:10])
    day = int(nameoffile[10:12])

# Stations with 6 files
    if station(nameoffile) == 'vlzl':
        if between(year, month, day, 2006, 9, 26, 2010, 1, 30):
            return 'VLZL_teqc_20060926.conf'
        elif between(year, month, day, 2010, 1, 30, 2010, 7, 2):
            return 'VLZL_teqc_20100130.conf'
        elif between(year, month, day, 2010, 7, 2, 2011, 1, 17):
            return 'VLZL_teqc_20100702.conf'
        elif between(year, month, day, 2011, 1, 17, 2011, 7, 4):
            return 'VLZL_teqc_20110117.conf'
        elif between(year, month, day, 2011, 7, 14, 2014, 6, 5):
            return 'VLZL_teqc_20110714.conf'
        elif past(year, month, day, 2014, 6, 5):
            return 'VLZL_teqc_20140605.conf'
# Stations with 5 files
    elif station(nameoffile) == 'atjn':
        if between(year, month, day, 2005, 10, 26, 2011, 1, 18):
            return 'ATJN_teqc_20051026.conf'
        elif between(year, month, day, 2011, 1, 18, 2011, 7, 19):
            return 'ATJN_teqc_20110118.conf'
        elif between(year, month, day, 2011, 7, 19, 2013, 5, 13):
            return 'ATJN_teqc_20110719.conf'
        elif between(year, month, day, 2013, 5, 14, 2014, 5, 1):
            return 'ATJN_teqc_20130514.conf'
        elif past(year, month, day, 2014, 5, 1):
            return 'ATJN_teqc_20140501.conf'
    elif station(nameoffile) == 'cdlc':
        if past(year, month, day, 2006, 10, 5):
            return 'CDLC_teqc_20061005.conf'
        elif between(year, month, day, 2010, 5, 31, 2011, 1, 22):
            return 'CDLC_teqc_20100531.conf'
        elif between(year, month, day, 2011, 1, 22, 2013, 2, 5):
            return 'CDLC_teqc_20110122.conf'
        elif between(year, month, day, 2013, 2, 5, 2014, 7, 21):
            return 'CDLC_teqc_20130205.conf'
        elif past(year, month, day, 2014, 7, 21):
            return 'CDLC_teqc_20140721.conf'
    elif station(nameoffile) == 'cgtc':
        if between(year, month, day, 2009, 8, 23, 2009, 8, 30):
            return 'CGTC_teqc_20090823.conf'
        elif between(year, month, day, 2009, 8, 30, 2011, 1, 14):
            return 'CGTC_teqc_20090830.conf'
        elif between(year, month, day, 2011, 1, 14, 2013, 2, 3):
            return 'CGTC_teqc_20110114.conf'
        elif between(year, month, day, 2013, 2, 3, 2014, 4, 4):
            return 'CGTC_teqc_20130203.conf'
        elif past(year, month, day, 2014, 4, 4):
            return 'CGTC_teqc_20140404.conf'
    elif station(nameoffile) == 'clla':
        if between(year, month, day, 2008, 8, 1, 2009, 4, 6):
            return 'CLLA_teqc_20140801.conf'
        elif between(year, month, day, 2009, 4, 6, 2011, 1, 14):
            return 'CLLA_teqc_20090406.conf'
        elif between(year, month, day, 2011, 1, 14, 2011, 8, 1):
            return 'CLLA_teqc_20110114.conf'
        elif between(year, month, day, 2011, 8, 1, 2014, 5, 15):
            return 'CLLA_teqc_20110801.conf'
        elif past(year, month, day, 2014, 5, 15):
            return 'CLLA_teqc_20140515.conf'
    elif station(nameoffile) == 'pccl':
        if between(year, month, day, 2005, 10, 19, 2006, 10, 10):
            return 'PCCL_teqc_20051019.conf'
        elif between(year, month, day, 2006, 10, 10, 2011, 7, 3):
            return 'PCCL_teqc_20061010.conf'
        elif between(year, month, day, 2011, 7, 3, 2013, 1, 30):
            return 'PCCL_teqc_20110703.conf'
        elif between(year, month, day, 2013, 1, 30, 2013, 5, 13):
            return 'PCCL_teqc_20130130.conf'
        elif past(year, month, day, 2013, 5, 13):
            return 'PCCL_teqc_20130513.conf'
# Stations with 4 files
    elif station(nameoffile) == 'cbaa':
        if between(year, month, day, 2006, 10, 1, 2011, 1, 15):
            return 'CBAA_teqc_20061001.conf'
        elif between(year, month, day, 2011, 1, 15, 2011, 7, 14):
            return 'CBAA_teqc_20110115.conf'
        elif between(year, month, day, 2011, 7, 14, 2014, 5, 7):
            return 'CBAA_teqc_20110714.conf'
        elif past(year, month, day, 2014, 5, 7):
            return 'CBAA_teqc_20140507.conf'
    elif station(nameoffile) == 'chmz':
        if between(year, month, day, 2007, 7, 16, 2011, 1, 14):
            return 'CHMZ_teqc_20070716.conf'
        elif between(year, month, day, 2011, 1, 14, 2013, 2, 3):
            return 'CHMZ_teqc_20110114.conf'
        elif between(year, month, day, 2013, 2, 3, 2014, 1, 1):
            return 'CHMZ_teqc_20130203.conf'
        elif past(year, month, day, 2014, 1, 1):
            return 'CHMZ_teqc_20140101.conf'
    elif station(nameoffile) == 'cjnt':
        if past(year, month, day, 2005, 10, 31):
            return 'CJNT_teqc_20051031.conf'
        elif between(year, month, day, 2011, 7, 14, 2013, 2, 5):
            return 'CJNT_teqc_20110714.conf'
        elif between(year, month, day, 2013, 2, 5, 2013, 8, 8):
            return 'CJNT_teqc_20130205.conf'
        elif past(year, month, day, 2013, 8, 8):
            return 'CJNT_teqc_20130808.conf'
    elif station(nameoffile) == 'cons':
        if between(year, month, day, 2003, 4, 29, 2007, 12, 6):
            return 'CONS_teqc_20030429.conf'
        elif between(year, month, day, 2007, 12, 6, 2008, 4, 22):
            return 'CONS_teqc_20071206.conf'
        elif between(year, month, day, 2008, 4, 22, 2014, 1, 23):
            return 'CONS_teqc_20080422.conf'
        elif past(year, month, day, 2014, 1, 23):
            return 'CONS_teqc_20140123.conf'
    elif station(nameoffile) == 'jrgn':
        if between(year, month, day, 2005, 11, 5, 2010, 6, 3):
            return 'JRGN_teqc_20051105.conf'
        elif between(year, month, day, 2010, 6, 3, 2011, 1, 18):
            return 'JRGN_teqc_20100603.conf'
        elif between(year, month, day, 2011, 1, 18, 2014, 5, 8):
            return 'JRGN_teqc_20110118.conf'
        elif past(year, month, day, 2014, 5, 8):
            return 'JRGN_teqc_20140508.conf'
    elif station(nameoffile) == 'ptre':
        if between(year, month, day, 2005, 10, 23, 2011, 5, 1):
            return 'PTRE_teqc_20051023.conf'
        elif between(year, month, day, 2011, 5, 1, 2011, 6, 29):
            return 'PTRE_teqc_20110501.conf'
        elif between(year, month, day, 2011, 6, 29, 2013, 1, 31):
            return 'PTRE_teqc_20110629.conf'
        elif past(year, month, day, 2013, 1, 31):
            return 'PTRE_teqc_20130131.conf'
    elif station(nameoffile) == 'srgd':
        if between(year, month, day, 2006, 9, 29, 2007, 3, 14):
            return 'SRGD_teqc_20060929.conf'
        elif between(year, month, day, 2007, 3, 14, 2011, 2, 10):
            return 'SRGD_teqc_20070314.conf'
        elif between(year, month, day, 2011, 2, 10, 2014, 5, 5):
            return 'SRGD_teqc_20110210.conf'
        elif past(year, month, day, 2014, 5, 5):
            return 'SRGD_teqc_20140505.conf'
    elif station(nameoffile) == 'utar':
        if between(year, month, day, 2003, 11, 30, 2008, 11, 29):
            return 'UTAR_teqc_20031130.conf'
        elif between(year, month, day, 2008, 11, 29, 2009, 11, 18):
            return 'UTAR_teqc_20081129.conf'
        elif between(year, month, day, 2009, 11, 18, 2014, 4, 22):
            return 'UTAR_teqc_20091118.conf'
        elif past(year, month, day, 2014, 4, 22):
            return 'UTAR_teqc_20140422.conf'
# Stations with 3 files
    elif station(nameoffile) == 'aeda':
        if between(year, month, day, 2003, 9, 7, 2012, 11, 25):
            return 'AEDA_teqc_20030907.conf'
        elif between(year, month, day, 2012, 11, 25, 2014, 3, 31):
            return 'AEDA_teqc_20121125.conf'
        elif past(year, month, day, 2014, 3, 31):
            return 'AEDA_teqc_20140331.conf'
    elif station(nameoffile) == 'crsc':
        if between(year, month, day, 2005, 11, 8, 2011, 1, 14):
            return 'CRSC_teqc_20051108.conf'
        elif between(year, month, day, 2011, 1, 14, 2011, 7, 18):
            return 'CRSC_teqc_20110114.conf'
        elif past(year, month, day, 2011, 7, 18):
            return 'CRSC_teqc_20110718.conf'
    elif station(nameoffile) == 'crzl':
        if past(year, month, day, 2008, 11, 15):
            return 'CRZL_teqc_20081115.conf'
        elif between(year, month, day, 2011, 1, 14, 2011, 7, 18):
            return 'CRZL_teqc_20110114.conf'
        elif past(year, month, day, 2011, 7, 18):
            return 'CRZL_teqc_20110718.conf'
    elif station(nameoffile) == 'fbaq':
        if between(year, month, day, 2003, 9, 6, 2007, 1, 25):
            return 'FBAQ_teqc_20030906.conf'
        elif between(year, month, day, 2007, 1, 25, 2012, 11, 25):
            return 'FBAQ_teqc_20070125.conf'
        elif past(year, month, day, 2012, 11, 25):
            return 'FBAQ_teqc_20121125.conf'
    elif station(nameoffile) == 'lvil':
        if between(year, month, day, 2003, 7, 28, 2010, 5, 19):
            return 'LVIL_teqc_20030728.conf'
        elif between(year, month, day, 2010, 5, 19, 2013, 11, 29):
            return 'LVIL_teqc_20100519.conf'
        elif past(year, month, day, 2013, 11, 29):
            return 'LVIL_teqc_20131129.conf'
    elif station(nameoffile) == 'picc':
        if between(year, month, day, 2007, 11, 6, 2009, 11, 25):
            return 'PICC_teqc_20071106.conf'
        elif between(year, month, day, 2009, 11, 25, 2014, 4, 4):
            return 'PICC_teqc_20091125.conf'
        elif past(year, month, day, 2014, 4, 4):
            return 'PICC_teqc_20140404.conf'
    elif station(nameoffile) == 'port':
        if between(year, month, day, 2002, 12, 11, 2012, 4, 4):
            return 'PORT_teqc_20021211.conf'
        elif between(year, month, day, 2012, 4, 4, 2014, 10, 01):
            return 'PORT_teqc_20120404.conf'
        elif past(year, month, day, 2014, 10, 01):
            return 'PORT_teqc_20141001.conf'
    elif station(nameoffile) == 'slmc':
        if between(year, month, day, 2003, 7, 28, 2010, 3, 23):
            return 'SLMC_teqc_20030728.conf'
        elif between(year, month, day, 2010, 3, 23, 2014, 9, 30):
            return 'SLMC_teqc_20100323.conf'
        elif past(year, month, day, 2014, 9, 30):
            return 'SLMC_teqc_20140930.conf'
    elif station(nameoffile) == 'tolo':
        if between(year, month, day, 2005, 5, 11, 2007, 5, 6):
            return 'TOLO_teqc_20050511.conf'
        elif between(year, month, day, 2007, 5, 6, 2014, 8, 15):
            return 'TOLO_teqc_20070506.conf'
        elif past(year, month, day, 2014, 8, 15):
            return 'TOLO_teqc_20140815.conf'
    elif station(nameoffile) == 'uape':
        if between(year, month, day, 2005, 5, 19, 2009, 11, 17):
            return 'UAPE_teqc_20050519.conf'
        elif between(year, month, day, 2009, 11, 17, 2014, 4, 1):
            return 'UAPE_teqc_20091117.conf'
        elif past(year, month, day, 2014, 4, 1):
            return 'UAPE_teqc_20140401.conf'
    elif station(nameoffile) == 'ucnf':
        if between(year, month, day, 2005, 4, 29, 2008, 1, 3):
            return 'UCNF_teqc_20050429.conf'
        elif between(year, month, day, 2008, 1, 3, 2008, 1, 15):
            return 'UCNF_teqc_20080103.conf'
        elif past(year, month, day, 2008, 1, 15):
            return 'UCNF_teqc_20080115.conf'
    elif station(nameoffile) == 'vnev':
        if between(year, month, day, 2003, 1, 8, 2014, 1, 9):
            return 'VNEV_teqc_20030108.conf'
        elif between(year, month, day, 2014, 1, 9, 2014, 10, 2):
            return 'VNEV_teqc_20140109.conf'
        elif past(year, month, day, 2014, 10, 2):
            return 'VNEV_teqc_20141002.conf'
# Stations with 2 files
    elif station(nameoffile) == 'cern':
        if between(year, month, day, 2010, 3, 6, 2014, 8, 7):
            return 'CERN_teqc_20100306.conf'
        elif past(year, month, day, 2014, 8, 7):
            return 'CERN_teqc_20140807.conf'
    elif station(nameoffile) == 'cmba':
        if between(year, month, day, 2007, 5, 8, 2014, 9, 10):
            return 'CMBA_teqc_20070508.conf'
        elif past(year, month, day, 2014, 9, 10):
            return 'CMBA_teqc_20140910.conf'
    elif station(nameoffile) == 'colc':
        if between(year, month, day, 2007, 11, 4, 2009, 11, 20):
            return 'COLC_teqc_20071104.conf'
        elif past(year, month, day, 2009, 11, 20):
            return 'COLC_teqc_20091120.conf'
    elif station(nameoffile) == 'curi':
        if between(year, month, day, 2010, 3, 8, 2013, 6, 25):
            return 'CURI_teqc_20100308.conf'
        elif past(year, month, day, 2013, 6, 25):
            return 'CURI_teqc_20130625.conf'
    elif station(nameoffile) == 'dgf1':
        if between(year, month, day, 2004, 1, 1, 2012, 7, 6):
            return 'DGF1_teqc_20040101.conf'
        elif past(year, month, day, 2012, 7, 6):
            return 'DGF1_teqc_20120706.conf'
    elif station(nameoffile) == 'emat':
        if between(year, month, day, 2007, 5, 5, 2013, 11, 24):
            return 'EMAT_teqc_20070505.conf'
        elif past(year, month, day, 2013, 11, 24):
            return 'EMAT_teqc_20131124.conf'
    elif station(nameoffile) == 'ctlr':
        if past(year, month, day, 2007, 7, 12):
            return 'CTLR_teqc_20070712.conf'
        elif between(year, month, day, 2011, 1, 22, 2013, 2, 7):
            return 'CTLR_teqc_20110122.conf'
    elif station(nameoffile) == 'iloc':
        if between(year, month, day, 2010, 3, 9, 2013, 6, 26):
            return 'ILOC_teqc_20100309.conf'
        elif past(year, month, day, 2013, 6, 26):
            return 'ILOC_teqc_20130626.conf'
    elif station(nameoffile) == 'junt':
        if between(year, month, day, 2007, 12, 14, 2014, 9, 11):
            return 'JUNT_teqc_20071214.conf'
        elif past(year, month, day, 2014, 9, 11):
            return 'JUNT_teqc_20140911.conf'
    elif station(nameoffile) == 'lsch':
        if between(year, month, day, 2006, 11, 20, 2014, 8, 14):
            return 'LSCH_teqc_20061120.conf'
        elif past(year, month, day, 2014, 8, 14):
            return 'LSCH_teqc_20140814.conf'
    elif station(nameoffile) == 'maul':
        if between(year, month, day, 2003, 12, 22, 2007, 3, 13):
            return 'MAUL_teqc_20031222.conf'
        elif past(year, month, day, 2007, 3, 13):
            return 'MAUL_teqc_20070313.conf'
    elif station(nameoffile) == 'navi':
        if between(year, month, day, 2010, 3, 8, 2013, 6, 27):
            return 'NAVI_teqc_20100308.conf'
        elif past(year, month, day, 2013, 6, 27):
            return 'NAVI_teqc_20130627.conf'
    elif station(nameoffile) == 'ovll':
        if between(year, month, day, 2004, 4, 26, 2007, 5, 1):
            return 'OVLL_teqc_20040426.conf'
        elif past(year, month, day, 2007, 5, 1):
            return 'OVLL_teqc_20070501.conf'
    elif station(nameoffile) == 'pcal':
        if between(year, month, day, 2003, 11, 29, 2005, 3, 21):
            return 'PCAL_teqc_20031129.conf'
        elif past(year, month, day, 2005, 3, 21):
            return 'PCAL_teqc_20050321.conf'
    elif station(nameoffile) == 'pell':
        if between(year, month, day, 2010, 3, 9, 2013, 6, 24):
            return 'PELL_teqc_20100309.conf'
        elif past(year, month, day, 2013, 6, 24):
            return 'PELL_teqc_20130624.conf'
    elif station(nameoffile) == 'pfrj':
        if between(year, month, day, 2006, 11, 18, 2013, 11, 27):
            return 'PFRJ_teqc_20061118.conf'
        elif past(year, month, day, 2013, 11, 27):
            return 'PFRJ_teqc_20131127.conf'
    elif station(nameoffile) == 'picn':
        if between(year, month, day, 1999, 9, 18, 2003, 12, 3):
            return 'PICN_teqc_19990918.conf'
        elif past(year, month, day, 2003, 12, 3):
            return 'PICN_teqc_20031203.conf'
    elif station(nameoffile) == 'pmej':
        if between(year, month, day, 2003, 11, 29, 2009, 8, 29):
            return 'PMEJ_teqc_20031129.conf'
        elif past(year, month, day, 2009, 8, 29):
            return 'PMEJ_teqc_20090829.conf'
    elif station(nameoffile) == 'qlap':
        if between(year, month, day, 2010, 3, 10, 2012, 11, 29):
            return 'QLAP_teqc_20100310.conf'
        elif past(year, month, day, 2012, 11, 29):
            return 'QLAP_teqc_20121129.conf'
    elif station(nameoffile) == 'rado':
        if between(year, month, day, 2007, 12, 8, 2013, 10, 4):
            return 'RADO_teqc_20071208.conf'
        elif past(year, month, day, 2007, 5, 1):
            return 'RADO_teqc_20131004.conf'
    elif station(nameoffile) == 'rcsd':
        if between(year, month, day, 2008, 10, 16, 2013, 12, 17):
            return 'RCSD_teqc_20081016.conf'
        elif past(year, month, day, 2013, 12, 17):
            return 'RCSD_teqc_20131217.conf'
    elif station(nameoffile) == 'robl':
        if between(year, month, day, 2008, 10, 28, 2013, 9, 9):
            return 'ROBL_teqc_20081028.conf'
        elif past(year, month, day, 2013, 9, 9):
            return 'ROBL_teqc_20130909.conf'
    elif station(nameoffile) == 'sjav':
        if between(year, month, day, 2003, 6, 24, 2007, 12, 5):
            return 'SJAV_teqc_20030624.conf'
        elif past(year, month, day, 2007, 12, 5):
            return 'SJAV_teqc_20071205.conf'
    elif station(nameoffile) == 'valn':
        if between(year, month, day, 2005, 1, 1, 2013, 7, 31):
            return 'VALN_teqc_20050101.conf'
        elif past(year, month, day, 2013, 7, 31):
            return 'VALN_teqc_20130731.conf'
    elif station(nameoffile) == 'vita':
        if between(year, month, day, 2010, 3, 11, 2013, 5, 29):
            return 'VITA_teqc_20100311.conf'
        elif past(year, month, day, 2013, 5, 29):
            return 'VITA_teqc_20130529.conf'
    elif station(nameoffile) == 'zapa':
        if between(year, month, day, 2010, 3, 7, 2013, 11, 19):
            return 'ZAPA_teqc_20100307.conf'
        elif past(year, month, day, 2013, 11, 19):
            return 'ZAPA_teqc_20131119.conf'
    elif station(nameoffile) == 'pcha':
        if between(year, month, day, 2005, 6, 28, 2009, 11, 18):
            return 'PCHA_teqc_20050628.conf'
        elif past(year, month, day, 2009, 11, 18):
            return 'PCHA_teqc_20091118.conf'
# Stations with 1 file
    elif station(nameoffile) == 'adat':
        if past(year, month, day, 2013, 9, 11):
            return 'ADAT_teqc_20130911.conf'
    elif station(nameoffile) == 'antc':
        if past(year, month, day, 2014, 8, 23):
            return 'ANTC_teqc_20140823.conf'
    elif station(nameoffile) == 'arjf':
        if past(year, month, day, 2013, 9, 16):
            return 'ARJF_teqc_20130916.conf'
    elif station(nameoffile) == 'bton':
        if past(year, month, day, 2006, 11, 16):
            return 'BT0N_teqc_2006116.conf'
    elif station(nameoffile) == 'cabr':
        if past(year, month, day, 2010, 3, 9):
            return 'CABR_teqc_20100309.conf'
    elif station(nameoffile) == 'cast':
        if past(year, month, day, 2013, 10, 26):
            return 'CAST_teqc_20131026.conf'
    elif station(nameoffile) == 'cgua':
        if past(year, month, day, 2013, 9, 13):
            return 'CGUA_teqc_20130913.conf'
    elif station(nameoffile) == 'chm2':
        if past(year, month, day, 2014, 4, 5):
            return 'CHM2_teqc_20140405.conf'
    elif station(nameoffile) == 'chyt':
        if past(year, month, day, 2014, 4, 24):
            return 'CHYT_teqc_20140424.conf'
    elif station(nameoffile) == 'cll1':
        if past(year, month, day, 2014, 8, 24):
            return 'CLL1_teqc_20140824.conf'
    elif station(nameoffile) == 'cnba':
        if past(year, month, day, 2006, 11, 21):
            return 'CNBA_teqc_20061121.conf'
    elif station(nameoffile) == 'coll':
        if past(year, month, day, 2004, 5, 26):
            return 'COLL_teqc_20040526.conf'
    elif station(nameoffile) == 'crrh':
        if past(year, month, day, 2013, 7, 26):
            return 'CRRH_teqc_20130726.conf'
    elif station(nameoffile) == 'csom':
        if past(year, month, day, 2013, 10, 23):
            return 'CSOM_teqc_20131023.conf'
    elif station(nameoffile) == 'hlne':
        if past(year, month, day, 2013, 10, 9):
            return 'HLNE_teqc_20131009.conf'
    elif station(nameoffile) == 'hmbs':
        if past(year, month, day, 2007, 3, 28):
            return 'HMBS_teqc_20070328.conf'
    elif station(nameoffile) == 'lemu':
        if past(year, month, day, 2010, 3, 7):
            return 'LEMU_teqc_20100307.conf'
    elif station(nameoffile) == 'llch':
        if past(year, month, day, 2014, 2, 7):
            return 'LLCH_teqc_20140207.conf'
    elif station(nameoffile) == 'lmel':
        if past(year, month, day, 2012, 4, 12):
            return 'LMEL_teqc_20120412.conf'
    elif station(nameoffile) == 'lnqm':
        if past(year, month, day, 2014, 8, 23):
            return 'LNQM_teqc_20140823.conf'
    elif station(nameoffile) == 'imch':
        if past(year, month, day, 2013, 8, 24):
            return 'IMCH_teqc_20130824.conf'
    elif station(nameoffile) == 'mica':
        if past(year, month, day, 2007, 12, 16):
            return 'MICA_teqc_20071216.conf'
    elif station(nameoffile) == 'mnmi':
        if past(year, month, day, 2007, 3, 28):
            return 'MNMI_teqc_20070328.conf'
    elif station(nameoffile) == 'mrcg':
        if past(year, month, day, 2014, 2, 2):
            return 'MRCG_teqc_20140202.conf'
    elif station(nameoffile) == 'pazu':
        if past(year, month, day, 2013, 9, 13):
            return 'PAZU_teqc_20130913.conf'
    elif station(nameoffile) == 'pecl':
        if past(year, month, day, 2014, 8, 21):
            return 'PECL_teqc_20140821.conf'
    elif station(nameoffile) == 'pedr':
        if past(year, month, day, 2006, 11, 20):
            return 'PEDR_teqc_20061120.conf'
    elif station(nameoffile) == 'plaj':
        if past(year, month, day, 2010, 3, 16):
            return 'PLAJ_teqc_20100316.conf'
    elif station(nameoffile) == 'plvp':
        if past(year, month, day, 2014, 8, 21):
            return 'PLVP_teqc_20140821.conf'
    elif station(nameoffile) == 'prnl':
        if past(year, month, day, 2014, 6, 29):
            return 'PRNL_teqc_20140629.conf'
    elif station(nameoffile) == 'psga':
        if past(year, month, day, 2007, 3, 29):
            return 'PSGA_teqc_20070329.conf'
    elif station(nameoffile) == 'pwil':
        if past(year, month, day, 2013, 11, 1):
            return 'PWIL_teqc_20131101.conf'
    elif station(nameoffile) == 'qlln':
        if past(year, month, day, 2013, 7, 25):
            return 'QLLN_teqc_20130725.conf'
    elif station(nameoffile) == 'quil':
        if past(year, month, day, 2003, 9, 7):
            return 'QUIL_teqc_20030907.conf'
    elif station(nameoffile) == 'sill':
        if past(year, month, day, 2008, 6, 13):
            return 'SILL_teqc_20080613.conf'
    elif station(nameoffile) == 'tamr':
        if past(year, month, day, 2013, 9, 11):
            return 'TAMR_teqc_20130911.conf'
#   elif station(nameoffile) == 'tcpl':
#        if past(year, month, day, 2013, 9, 13):
#            return 'TCPL_teqc_xxxxxxxx.conf'
    elif station(nameoffile) == 'tmco':
        if past(year, month, day, 2014, 8, 22):
            return 'TMCO_teqc_20140822.conf'
    elif station(nameoffile) == 'toco':
        if past(year, month, day, 2003, 9, 7):
            return 'TOCO_teqc_20030907.conf'
    elif station(nameoffile) == 'trst':
        if past(year, month, day, 2014, 2, 9):
            return 'TRST_teqc_20140209.conf'
    elif station(nameoffile) == 'ttal':
        if past(year, month, day, 2014, 5, 20):
            return 'TTAL_teqc_20140520.conf'
    elif station(nameoffile) == 'uapf':
        if past(year, month, day, 1999, 9, 11):
            return 'UAPF_teqc_19990911.conf'
    elif station(nameoffile) == 'ucna':
        if past(year, month, day, 2003, 11, 29):
            return 'UCNA_teqc_20031129.conf'
    elif station(nameoffile) == 'urcu':
        if past(year, month, day, 2007, 12, 4):
            return 'URCU_teqc_20071204.conf'
    elif station(nameoffile) == 'vall':
        if past(year, month, day, 2008, 6, 12):
            return 'VALL_teqc_20080612.conf'
    elif station(nameoffile) == 'vohg':
        if past(year, month, day, 2014, 2, 14):
            return 'VOHG_teqc_20140214.conf'
    return ""