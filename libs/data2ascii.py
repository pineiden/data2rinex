import sys, os, time

def fileExt(files): # Obtiene la extension de cada archivo (3 caracteres al final)
    return files[-3:].lower()

def data2ascii(path):
	if os.access(path, os.F_OK):
		#Iterando en path; rescatando los archivos 'files'
	    for root, dirs, files in os.walk(path):
	    	#Iterando en cada lista de archivos
	        for eachfile in files:
	            if fileExt(eachfile) in ["t00", "t01", "t02"]:
	                print eachfile
	                #llama a ejecucion 'runpkr00'
	                os.system("cd " + root + ' && runpkr00 -d ' + eachfile)

	#registrar tiempo de operacion