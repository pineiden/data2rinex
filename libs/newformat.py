# input = estaYYYYMMDD.ext ; output = estaDOY0.yyo

def isLeapYear(year):
    if year % 4 == 0:
        if year % 100 == 0:
            if year % 400 == 0:
                return 1
            return 0
        return 1
    return 0

def daysOfMonth(year, month):
    if month == 2:
        if isLeapYear(year):
            return 29
        return 28
    elif month in (1, 3, 5, 7, 8, 10, 12):
        return 31
    return 30

def dayOfYear(year, month, day):
    doy = day
    for month in range(1, month):
        doy += daysOfMonth(year, month)
    if doy < 10:
        return "00" + str(doy)
    elif doy < 100:
        return "0" + str(doy)
    return doy


def newformatname(nameoffile):

    year = int(nameoffile[4:8])
    month = int(nameoffile[8:10])
    day = int(nameoffile[10:12])

    return nameoffile[0:4]+str(dayOfYear(year, month, day))+"0."+str(year)[2:4]+"o".lower()